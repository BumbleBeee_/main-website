<!DOCTYPE html>
<html lang="en">
<style>
	.stats {
		background-color: #4CAF50;
	}
	.stats:hover {
		background-color: #4CAF50 !important;
	}
</style>
<?php

if (!isset($_GET['plugin'])) {
	header("Location: index.php");
	die();
}
$plugin = $_GET['plugin'];

?>
<head>
	<meta charset="UTF-8">
	<title><?php echo $plugin; ?> Stats | Bumblebee.ml</title>
	<link rel="stylesheet" href="../files/css/styles.css">
</head>
<body style="background-image: url('http://bumblebee.ml/files/css/bg2.png');">
	<?php
	include("../files/navbar.php");
	include("../files/connect.php");
	$query = mysqli_query($con, "SELECT * FROM CachedStats WHERE Plugin='$plugin'");
	$data = mysqli_fetch_assoc($query);
	$active = mysqli_num_rows(mysqli_query($con, "SELECT * FROM Active WHERE Plugin='$plugin'"));
	?>
	<div style="padding-left: 10%; padding-top: 150px; text-align:center;">
		<div class="stat">
			<div class="statheader">Amount of active</div>
			<div class="statcontainer">
				<?php
				echo $active;
				?>
			</div>
			<div class="statfooter">servers this month</div>
		</div>

		<div class="stat">
			<div class="statheader">There are currently</div>
			<div class="statcontainer">
				<?php
				echo $data['TotalOnline'] . " / " . $data['TotalServers'];
				?>
			</div>
			<div class="statfooter">servers online</div>
		</div>

		<div class="stat">
			<div class="statheader">There are currently</div>
			<div class="statcontainer">
				<?php
				echo $data['TotalPlayers'] . " / " . $data['MaxPlayers'];
				?>
			</div>
			<div class="statfooter">players online</div>
		</div>

		<div style="width:100%; padding-left:15%; padding-top: 250px; text-align:center;">
			<div class="stat">
				<div class="statheader">bStats enabled for</div>
				<div class="statcontainer">
					<?php
					echo $data['bStatEnabled'];
					?>
				</div>
				<div class="statfooter">alltime servers</div>
			</div>
		</div>
		<div style="text-align:center;">
			<div class="stat">
				<div class="statheader">bStats disabled for</div>
				<div class="statcontainer">
					<?php
					$bstatDisable = $data['bStatEnabled']-$active;
					if ($bstatDisable < 0)
						$bstatDisable = $bstatDisable*-1;
					echo $bstatDisable;
					?>
				</div>
				<div class="statfooter">alltime servers</div>
			</div>
		</div>
	</div>
</body>
</html>
