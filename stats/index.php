<!DOCTYPE html>
<html lang="en">
<style>
	.stats {
		background-color: #4CAF50;
	}
	.stats:hover {
		background-color: #4CAF50 !important;
	}

  .stat:hover {
    color: #006666 !important;
  }
</style>
<head>
	<meta charset="UTF-8">
	<title>Plugin Stats | Bumblebee.ml</title>
	<link rel="stylesheet" href="../files/css/styles.css">
</head>
<body style="background-image: url('http://bumblebee.ml/files/css/bg2.png');">
	<?php
	include("../files/navbar.php");
	?>
	<div style="padding-left: 20%; padding-top: 150px; text-align:center;">
    <a href="stats.php?plugin=Morph" style="color: black;">
  		<div class="stat" style="width:30%;">
  			<div class="statheader">&nbsp;</div>
  			<div class="statcontainer">
    	     Morph
  			</div>
  			<div class="statfooter">Click to view</div>
  		</div>
    </a>

    <a href="stats.php?plugin=RailMiner" style="color: black;">
  		<div class="stat" style="width:30%;">
  			<div class="statheader">&nbsp;</div>
  			<div class="statcontainer">
  				RailMiner
  			</div>
  			<div class="statfooter">Click to view</div>
  		</div>
    </a>
	</div>
</body>
</html>
