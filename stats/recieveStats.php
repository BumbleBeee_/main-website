<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Receive Plugin Stats</title>
</head>
<body>
<?php
	$agent = $_SERVER['HTTP_USER_AGENT'];
	if ($agent === "SubmitMorphStats2") {
		include("../files/connect.php");
	
		$plugin = $_POST['Plugin'];
		$guid = $_POST['GUID'];
		$os = $_POST['OS'];
		$osVer = $_POST['OSVersion'];
		$arch = $_POST['Arch'];
		$cores = $_POST['Cores'];
		$serverVer = $_POST['ServerVersion'];
		$javaVer = $_POST['JavaVersion'];
		$players = $_POST['Players'];
		$pluginVer = $_POST['PluginVersion'];
		$onlineMode = $_POST['OnlineMode'];
		$status = $_POST['Status'];
		$time = time();
		$bstats = $_POST['bStats'];

		$serverExists = mysqli_query($con, "SELECT GUID FROM Stats WHERE GUID='" . $guid . "'");
		$activeExists = mysqli_query($con, "SELECT GUID FROM Active WHERE GUID='" . $guid . "' AND Plugin='$plugin'");
		//Add send errors to email
		if (mysqli_num_rows($serverExists) < 1) {
			$add = "INSERT INTO Stats (Plugin, GUID, OS, OSVersion, Arch, Cores, ServerVersion, JavaVersion, Players, 
				PluginVersion, OnlineMode, Status, LastUpdated, bStats) VALUES ('$plugin', '$guid', '$os', '$osVer', '$arch', '$cores', '$serverVer', '$javaVer', '$players', 
				'$pluginVer', '$onlineMode', '$status', '$time', '$bstats')";
			if (mysqli_query($con, $add) !== TRUE) {
				echo "Failed to add stats!";
			}
			if (mysqli_query($con, "INSERT INTO Active (Plugin,GUID) VALUES ('$plugin', '$guid')") !== TRUE) {
				echo "Failed to add to active!";
			}
		} else {
			$update = "UPDATE Stats SET OS='$os', OSVersion='$osVer', Arch='$arch', Cores='$cores', ServerVersion='$serverVer', 
			JavaVersion='$javaVer', Players='$players', PluginVersion='$pluginVer', OnlineMode='$onlineMode', Status='$status', LastUpdated='$time', bStats='$bstats' WHERE GUID='$guid'";
			if (mysqli_query($con, $update) !== TRUE) {
				echo "Failed to add stats!";
			}

			if (mysqli_num_rows($activeExists) < 1) {
				if (mysqli_query($con, "INSERT INTO Active (Plugin,GUID) VALUES ('$plugin', '$guid')") !== TRUE) {
					echo "Failed to add to active!";
				}
			}
		}
	}
	?>
</body>
</html>
