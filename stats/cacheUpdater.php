<?php
include("/var/www/html/files/connect.php");

// Morph
$morph_cachedData = mysqli_fetch_assoc(mysqli_query($con, "SELECT * FROM CachedStats WHERE Plugin='Morph'"));
$morph_onlinePlayers = 0;
$morph_maxPlayers = $morph_cachedData['MaxPlayers'];
$morph_onlineServers = 0;
$morph_maxServers = $morph_cachedData['TotalServers'];
$morph_bStatEnabled = 0;

$morph_onlineData = mysqli_query($con, "SELECT * FROM Stats WHERE Status='Online' AND Plugin='Morph'");
$morph_allData = mysqli_query($con, "SELECT * FROM Stats WHERE Plugin='Morph'");

while ($morph_row = mysqli_fetch_assoc($morph_onlineData)) {
	$morph_onlinePlayers += $morph_row['Players'];
	$morph_onlineServers++;
}
while ($morph_row = mysqli_fetch_assoc($morph_allData)) {
	if ($morph_row['bStats'] === "1") {
		$morph_bStatEnabled++;
	}
}

if ($morph_onlinePlayers > $morph_maxPlayers) {
  $morph_maxPlayers = $morph_onlinePlayers;
}
if ($morph_onlineServers > $morph_maxServers) {
  $morph_maxServers = $morph_onlineServers;
}

//echo "Morph: " . $morph_onlinePlayers . " " . $morph_maxPlayers . " " . $morph_onlineServers . " " . $morph_maxServers;
$morph_query = "UPDATE CachedStats SET TotalPlayers='$morph_onlinePlayers', MaxPlayers='$morph_maxPlayers', TotalOnline='$morph_onlineServers', TotalServers='$morph_maxServers', bStatEnabled='$morph_bStatEnabled' WHERE Plugin='Morph'";
if (mysqli_query($con, $morph_query) !== TRUE) {
  mail("ummm6000@gmail.com", "Error updating morph stats", "Failed to update cache for morph stats");
}

// RailMiner
$railminer_cachedData = mysqli_fetch_assoc(mysqli_query($con, "SELECT * FROM CachedStats WHERE Plugin='RailMiner'"));
$railminer_onlinePlayers = 0;
$railminer_maxPlayers = $railminer_cachedData['MaxPlayers'];
$railminer_onlineServers = 0;
$railminer_maxServers = $railminer_cachedData['TotalServers'];
$railminer_bStatEnabled = 0;

$railminer_onlineData = mysqli_query($con, "SELECT * FROM Stats WHERE Status='Online' AND Plugin='RailMiner'");
$railminer_allData = mysqli_query($con, "SELECT * FROM Stats WHERE Plugin='RailMiner'");

while ($railminer_row = mysqli_fetch_assoc($railminer_onlineData)) {
  $railminer_onlinePlayers += $railminer_row['Players'];
  $railminer_onlineServers++;
}
while ($railminer_row = mysqli_fetch_assoc($railminer_allData)) {
	if ($railminer_row['bStats'] === "1") {
		$railminer_bStatEnabled++;
	}
}


if ($railminer_onlinePlayers > $railminer_maxPlayers) {
  $railminer_maxPlayers = $railminer_onlinePlayers;
}
if ($railminer_onlineServers > $railminer_maxServers) {
  $railminer_maxServers = $railminer_onlineServers;
}

//echo "RailMiner: " . $railminer_onlinePlayers . " " . $railminer_maxPlayers . " " . $railminer_onlineServers . " " . $railminer_maxServers;
$railminer_query = "UPDATE CachedStats SET TotalPlayers='$railminer_onlinePlayers', MaxPlayers='$railminer_maxPlayers', TotalOnline='$railminer_onlineServers', TotalServers='$railminer_maxServers', bStatEnabled='$railminer_bStatEnabled' WHERE Plugin='RailMiner'";
if (mysqli_query($con, $railminer_query) !== TRUE) {
  mail("ummm6000@gmail.com", "Error updating railminer stats", "Failed to update cache for railminer stats");
}

?>
