var counter = 0;

function addInput(divName) {
    var newdiv = document.createElement('table');

    newdiv.setAttribute("width", "100%");
    newdiv.innerHTML = '<tr><td>Slot</td><td><input type="number" name="slots[]"></td></tr><tr><td>Material</td><td><input type="text" name="materials[]"></td></tr><tr><td>Data</td><td><input type="number" name="data[]"></td></tr><tr><td>Display Name</td><td><input type="text" name="names[]"></td></tr><tr><td>Lore</td><td><input type="text" name="lores[]"></td></tr><tr><td>Enchanted</td><td><input type="text" name="enchanted[]"></td></tr><tr><td>Opens Inventory</td><td><input type="text" name="opens[]"></td></tr><tr><td>Closes?</td><td><input type="text" name="closes[]"></td></tr><tr><td>Executes as player</td><td><input type="text" name="execute[]"></td></tr><tr><td>Executes as console</td><td><input type="text" name="consoleExecute[]"></td></tr><tr><td><br></td><td></td></tr>';

    document.getElementById(divName).appendChild(newdiv);
    counter++;
}