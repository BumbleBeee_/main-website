<!DOCTYPE html>

<?php 

	if (isset($_GET['lines'])) {
		$line = $_GET['lines'];
	} else {
		header("Location: index.html");
	}

	function generateRandomString($length = 5) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

	if (isset($_POST['inventoryName'])) {

		$displayName = $_POST["displayName"];
		$inventoryName = $_POST["inventoryName"];
		$slots = $_POST['slots'];
		$materials = $_POST['materials'];
		$data = $_POST['data'];
		$names = $_POST['names'];
		$lores = $_POST['lores'];
		$enchanted = $_POST['enchanted'];
		$opens = $_POST['opens'];
		$closes = $_POST['closes'];
		$execute = $_POST['execute'];
		$consoleExecute = $_POST['consoleExecute'];

		$fname = "output/" . generateRandomString() . "." . generateRandomString();
		$f = fopen($fname, "w") or die("Unable to open file!");

		fwrite($f, "inv:\n");
		fwrite($f, "  " . $inventoryName . ":\n");
		fwrite($f, "    name: " . $displayName . "\n");
		fwrite($f, "    lines: " . $line . "\n");
		fwrite($f, "    slots:\n");

		for ($i = 0; $i < sizeof($slots); $i++) {
			$slot = $slots[$i];
			$mat = $materials[$i];
			$meta = $data[$i];
			$name = $names[$i];
			$lore = $lores[$i];
			$enchant = $enchanted[$i];
			$open = $opens[$i];
			$close = $closes[$i];
			$exe = $execute[$i];
			$cExe = $consoleExecute[$i];

			fwrite($f, "      '" . $slot . "':\n");
			fwrite($f, "        material: " . $mat . "\n");
			fwrite($f, "        display: '" . $name . "'\n");
			fwrite($f, "        lore:\n");
			foreach (explode('\n', $lore) as $l) {
				fwrite($f, "        - '" . $l . "'\n");
			}
			fwrite($f, "        enchanted: " . $enchant . "\n");
			fwrite($f, "        open: " . $open . "\n");
			fwrite($f, "        close: " . $close . "\n");
			fwrite($f, "        execute:\n");
			foreach (explode('\n', $exe) as $e) {
				fwrite($f, "        - '" . $e . "'\n");
			}
			fwrite($f, "        consoleExecute:\n");
			foreach (explode('\n', $cExe) as $c) {
				fwrite($f, "        - '" . $c . "'\n");
			}

		}

		fclose($f);
		header("Location: " . $fname);
	}
	
?>

<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Magic Inventory | Inventory Builder</title>
  <script src="files/js/addInput.js" language="Javascript" type="text/javascript"></script>
</head>
<body style="background-color: #262626; padding-left: 2%;">

<div style="float:left; width:50%;">
	<center>
		<?php
			echo '<img style="padding-top: 40%;" width="90%" height="90%" src="files/images/numberedlines' . $line . '.png">'
		?>
	</center>
</div>


<div style="float:left; width:50%; padding-top: 5%; ">
		<div style="height:80vh; width:100%; overflow: auto; color: white;">
 			<form method="POST" width="100%">

	         <div style="float: left; padding-left: 5%" width="100%" id="dynamicInput">
				<table width="100%">
					<tr>
						<td>Inventory Display Name</td>
						<td><input type="text" name="displayName"></td>
					</tr>
					<tr>
						<td>Inventory File Name</td>
						<td><input type="text" name="inventoryName"></td>
					</tr>
					<tr><td><br><br></td><td></td></tr>
					<tr>
						<td>Slot</td>
						<td><input type="number" name="slots[]"></td>
					</tr>
					<tr>
	                    <td>Material</td>
	                    <td><input type="text" name="materials[]"></td>
	                </tr>
	                <tr>
	                    <td>Data</td>
	                    <td><input type="number" name="data[]"></td>
	                </tr>
	                <tr>
	                    <td>Display Name</td>
	                    <td><input type="text" name="names[]"></td>
	                </tr>
	                <tr>
	                    <td>Lore</td>
	                    <td><input type="text" name="lores[]"></td>
	                </tr>
	                <tr>
	                    <td>Enchanted</td>
	                    <td><input type="text" name="enchanted[]"></td>
	                </tr>
	                <tr>
	                    <td>Opens Inventory</td>
	                    <td><input type="text" name="opens[]"></td>
	                </tr>
	                <tr>
	                    <td>Closes?</td>
	                    <td><input type="text" name="closes[]"></td>
	                </tr>
	                <tr>
	                    <td>Executes as player</td>
	                    <td><input type="text" name="execute[]"></td>
	                </tr>
	                <tr>
	                    <td>Executes as console</td>
	                    <td><input type="text" name="consoleExecute[]"></td>
	                </tr>
	                <tr><td><br></td><td></td></tr>
				</table>
        	 </div>
	</div>
         <input type="button" style="width:100%;" value="Add Item" onClick="addInput('dynamicInput');">
         <input type="submit" style="width:100%; margin-top: 2%;" value="Submit">
    </form>



   
</div>


</body>
</html>