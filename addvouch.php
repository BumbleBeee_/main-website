<!DOCTYPE html>
<html lang="en">
<style>
	.addvouch {
		background-color: #4CAF50;
	}
	.addvouch:hover {
		background-color: #4CAF50 !important;
	}

</style>
<head>
	<meta charset="UTF-8">
	<title>Add A Vouch | Bumblebee.ml</title>
	<link rel="stylesheet" href="files/css/styles.css">
	<link rel="stylesheet" href="files/css/contact.css">
</head>
<body style="background-image: url('files/css/bg2.png'); color: white;">
	<?php include("files/navbar.php"); ?>
	<?php include("files/connect.php"); ?>
	<div style="padding-left: 30%; padding-top: 150px;">
		
		<h1>Add A Vouch</h1>
		<p></p>
		<?php
		if (isset($_POST['submit'])) {
			if (isset($_POST['username']) && isset($_POST['name']) && sizeof(split(" ", $_POST['msg'])) > 1) {
				$username = $_POST['username'];
				$name = $_POST['name'];
				$ip = "";
				if (isset($_POST['ip'])) {
					$ip = $_POST['ip'];
				}
				$msg = mysqli_escape_string($con, $_POST['msg']);
				$send = $username . " Submited a vouch!\nMessage: " . wordwrap($msg, 70);
				
				$query = "INSERT INTO Vouches (Username, ServerName, ServerIP, Message, Status) VALUES 
				('$username', '$name', '$ip', '$msg', 'PENDING')";
				if (mysqli_query($con, $query) !== TRUE) {
					?>
					<p style="color: red;">Error: Failed to submit vouch!</p>
					<?php
				} else {
					?>
					<p style="color: green;">Thank you for submitting a vouch! Once it is approved it will appear on the Vouches page.</p>
					<?php
				}
				mail("ummm6000@gmail.com", "Vouch Submitted from Bumblebee.ml", $send);
			} else {
				?>
				<p style="color:red;">You must fill out all the fields</p>
				<?php
			}
		}
		?>
		<form action="addvouch.php" method="POST">
			<strong>Username</strong> <span style="color: gray">(required)</span> <br><input type="text" placeholder="Your username" name="username"><br>
			<br>
			<strong>Server Name</strong> <span style="color: gray">(required)</span> <br><input type="text" name="name" placeholder="Your server name"><br>
			<br>
			<strong>Server IP</strong> <span style="color: gray"></span> <br><input type="text" name="ip" placeholder="Your server IP"><br>
			<br>
			<strong>Message</strong> <span style="color: gray"></span> <br><textarea name="msg" cols="50" placeholder="Enter your message here.." rows="9"></textarea>
			<br>
			<br><br>
			<input name="submit" type="submit" value="Submit">
		</form>
	</div>
</body>
</html>