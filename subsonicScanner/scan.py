from apiclient.discovery import build
from apiclient.errors import HttpError
from oauth2client.tools import argparser
import mysql.connector
import os
import subprocess
import gdata.youtube
import gdata.youtube.service

DEVELOPER_KEY = "AIzaSyCMvzeaOXl-cKwBmS66VwIj8w_vMt9Yko4"
YOUTUBE_API_SERVICE_NAME = "youtube"
YOUTUBE_API_VERSION = "v3"

def fetch_all_youtube_videos(playlistId):
    youtube = build(YOUTUBE_API_SERVICE_NAME,
                    YOUTUBE_API_VERSION,
                    developerKey=DEVELOPER_KEY)
    res = youtube.playlistItems().list(
    part="snippet",
    playlistId=playlistId,
    maxResults="50"
    ).execute()

    nextPageToken = res.get('nextPageToken')
    while ('nextPageToken' in res):
        nextPage = youtube.playlistItems().list(
        part="snippet",
        playlistId=playlistId,
        maxResults="50",
        pageToken=nextPageToken
        ).execute()
        res['items'] = res['items'] + nextPage['items']

        if 'nextPageToken' not in nextPage:
            res.pop('nextPageToken', None)
        else:
            nextPageToken = nextPage['nextPageToken']

    return res

con = mysql.connector.connect(user='root',
                              password='user',
                              host='127.0.0.1',
                              database='nextcloudYoutube')
cursor = con.cursor();
users = {}

query = ("SELECT name, nextcloudUser FROM ncyt_users")
cursor.execute(query)
for (name, nextcloudUser) in cursor:
    users[name] = nextcloudUser

query = ("SELECT * FROM ncyt_playlists")
cursor.execute(query);

for (ID, user, name, link, destFolder) in cursor:
    # print("Playlist ID: {}, user: {}, playlist: {}, link: {}, folder: {}, ncUser: {}".format(ID,user,name,link,destFolder, users[user]))
    # Execute youtube-dl to directory(get full path with username from other db) /srv/Music/username/destFolder/
    # Delete all videos
    # rescan
    filePath = "/srv/Music/" + users[user] + "/" + destFolder + "/"

    folder = os.path.dirname(filePath)
    if not os.path.exists(folder):
        os.makedirs(folder)

# cmd = "youtube-dl --extract-audio --audio-format mp3 --console-title --audio-quality 0 --output " + filePath + "%(title)s.%(ext)s " + link
# subprocess.Popen(cmd.split())
    playlistID = split("list=", link)[1];
    print(playlistID)
    videos = fetch_all_youtube_videos(playlistID)

cursor.close();
con.close();
