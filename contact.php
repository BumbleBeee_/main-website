<!DOCTYPE html>
<html lang="en">
<style>
	.contact {
		background-color: #4CAF50;
	}
	.contact:hover {
		background-color: #4CAF50 !important;
	}
</style>
<head>
	<meta charset="UTF-8">
	<title>Contact Me | Bumblebee.ml</title>
	<link rel="stylesheet" href="files/css/styles.css">
	<link rel="stylesheet" href="files/css/contact.css">
</head>
<body style="background-image: url('files/css/bg2.png'); color: white;">
	<?php include("files/navbar.php"); ?>
	<?php include("files/connect.php"); ?>
	<div style="padding-left: 30%; padding-top: 150px;">
		
		<h1>Contact Me</h1>
		<p></p>
		<p>Contact me if you wish to enquire about getting work done!</p>
		<?php
		if (isset($_POST['submit'])) {
			if (isset($_POST['name']) && isset($_POST['email']) && sizeof(explode(" ", $_POST['msg'])) > 1) {
				$name = $_POST['name'];
				$email = $_POST['email'];
				$msg = $_POST['msg'];
				$send = "Name: " . $name . "\nEmail: " . $email . "\nMessage: " . wordwrap($msg, 70);


				mail("ummm6000@gmail.com", "Contact from bumblebee.ml", $send);
				$query = "INSERT INTO Contact (Name, Email, Message) VALUES ('$name', '$email', '$msg')";
				if (mysqli_query($con, $query) !== TRUE) {
					?>
					<p style="color: red;">Error: Failed to submit contact form!</p>
					<?php
				} else {
					?>
					<p style="color: green;">Thank you for contacting me! I will get back to you as soon as possible.</p>
					<?php
				}
			} else {
				?>
				<p style="color:red;">You must fill out all the fields</p>
				<?php
			}
		}
		?>
		<form action="contact.php" method="POST">
			<strong>Name</strong> <span style="color: gray">(required)</span> <br><input type="text" placeholder="Your name" name="name"><br>
			<br>
			<strong>Email/Skype</strong> <span style="color: gray">(required)</span> <br><input type="text" name="email" placeholder="Example@example.com"><br>
			<br>
			<strong>Message</strong> <span style="color: gray">(required)</span> <br><textarea name="msg" cols="50" placeholder="Enter your message here.." rows="9"></textarea><br>
			<br><br>
			<input name="submit" type="submit" value="Submit">
		</form>
	</div>
</body>
</html>
