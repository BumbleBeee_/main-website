<!DOCTYPE html>
<html lang="en">
<style>
	.vouch {
		background-color: #4CAF50;
	}
	.vouch:hover {
		background-color: #4CAF50 !important;
	}
</style>
<head>
	<meta charset="UTF-8">
	<title>Vouches | Bumblebee.ml</title>
	<link rel="stylesheet" href="files/css/styles.css">
</head>
<body style="background-image: url('files/css/bg2.png'); color: white;">
	<?php include("files/navbar.php") ?>
	<?php include("files/connect.php") ?>
	<div class="vouchwrapper">
		<div class="vouches">
			<table width="100%">

				<?php

				$page = $_GET['page'];
				if ($page === NULL) {
					$page = 1;
				}
				$offset = ($page*4)-4;

				$query = "SELECT * FROM Vouches WHERE Status='ACCEPTED' LIMIT " . $offset . ", 5";
				$result = mysqli_query($con, $query);

				$maxQuery = "SELECT ID FROM Vouches WHERE STATUS='ACCEPTED'";
				$maxResult = mysqli_query($con, $maxQuery);
				$maxPages = ceil((mysqli_num_rows($maxResult)/4));

					while ($row = mysqli_fetch_assoc($result)) {
						?>
						<tr class="vouchMsg">
							<td>
								Username: <?php echo $row['Username'] ?>
								<br><br>
								Server Name: <?php echo $row['ServerName'] ?>
								<br><br>
								Server IP: <?php echo $row['ServerIP'] ?>
								<br><br>
								Message: <?php echo $row['Message'] ?>
							</td>
						</tr>
						<tr style="height: 10px;"></tr>
						<?php
					}

				?>
			</table>
			<br>
			<br>
			<br>
			<table style="position: absolute; left: 0; bottom: 0;">
				<tr style="background-color: black;">
					<?php
					for ($i = 1; $i <= $maxPages; $i++) {
						echo "<td><a style=\"text-decoration: none; color: white;\" href=\"vouches.php?page=" . $i . "\"><div class=\"page\">" . $i . "</div></a></td>";
					}
					?>
				</tr>
			</table>
		</div>
	</div>
</body>
</html>
