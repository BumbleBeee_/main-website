<!DOCTYPE html>
<html lang="en">
<style>
	.home {
		background-color: #4CAF50;
	}
	.home:hover {
		background-color: #4CAF50 !important;
	}
</style>
<head>
	<meta charset="UTF-8">
	<title>Downloads | Bumblebee.ml</title>
	<link rel="stylesheet" href="files/css/styles.css">
</head>
<body style="background-image: url('files/css/bg2.png'); color: white;">
	<?php include("files/navbar.php") ?>
	<?php include("files/connect.php") ?>
	<div class="vouchwrapper">
		<div class="vouches">
			<?php
				$link = "plugins/Morph-".$_GET['ver'].".jar";
				if ($_GET['ver'] == "") {
					$link = "";
				}
				echo "<h1>Thank you for downloading Morph</h1>";
				echo "<p>Your download should start automatically in a second, If it does not <a style=\"text-decoration: none;color:white;\" href=\"" . $link . "\">click here</a></p>";
				echo "<iframe src=\"" . $link . "\" style=\"display:none;\"></iframe>";
			?>
		</div>
	</div>
</body>
</html>
