<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">
<style>
	.vouch {
		background-color: #4CAF50;
	}
	.vouch:hover {
		background-color: #4CAF50 !important;
	}
</style>
<head>
	<meta charset="UTF-8">
	<title>Vouches | Bumblebee.ml</title>
	<link rel="stylesheet" href="files/css/styles.css">
</head>
<body style="background-image: url('files/css/bg2.png'); color: white;">
	<?php
	if (!($_SESSION["username"] === "admin")) {
		header("Location: http://bumblebee.ml");
		die();
	}
	$_SESSION['returnurl'] = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
	include("files/adminnav.php");
	include("../files/connect.php")
	?>

	<div class="contactwrapper">
		<div class="contactme">
		<?php
		$page = $_GET['page'];
		if ($page === NULL) {
			$page = 1;
		}
		$offset = ($page*5)-5;

		$query = "SELECT * FROM Vouches LIMIT ". $offset . ", 6";
		$result = mysqli_query($con, $query);

		$maxQuery = "SELECT ID FROM Vouches";
		$maxResult = mysqli_query($con, $maxQuery);
		$maxPages = ceil((mysqli_num_rows($maxResult)/5));

			?>
			<table class="contactData" rules="none" style="width: 100%; table-layout:relative;">
				<thead>
					<tr>
						<td>ID</td>
						<td>Username</td>
						<td>ServerName</td>
						<td>IP</td>
						<td>Message</td>
						<td>Status</td>
						<td>Tools</td>
					</tr>
				</thead>
				<tbody>
					<?php
					while ($row = mysqli_fetch_assoc($result)) {
						echo "<tr class=\"contactData\">";
						echo "<td style=\"word-wrap: break-word\">" . $row['ID'] . "</td>";
						echo "<td style=\"word-wrap: break-word\">" . $row['Username'] . "</td>";
						echo "<td style=\"word-wrap: break-word\">" . $row['ServerName'] . "</td>";
						echo "<td style=\"word-wrap: break-word\">" . $row['ServerIP'] . "</td>";
						echo "<td style=\"word-wrap: break-word\">" . $row['Message'] . "</td>";
						echo "<td style=\"word-wrap: break-word\">" . $row['Status'] . "</td>";
						if ($row['Status'] === "PENDING") {
							echo "<td><a style=\"text-decoration: none;\" href=\"files/functions/acceptVouch.php?id=" . $row['ID'] . "\"><div class=\"acceptVouch\">Accept</div></a><a style=\"text-decoration: none;\" href=\"files/functions/denyVouch.php?id=" . $row['ID'] . "\"><div class=\"denyVouch\">Deny</div></a></td>";
						} else if ($row['Status'] == "ACCEPTED") {
							echo "<td><a style=\"text-decoration: none;\" href=\"files/functions/denyVouch.php?id=" . $row['ID'] . "\"><div class=\"denyVouch\">Remove</div></a></td>";
						} else if ($row['Status'] === "DENIED") {
							echo "<td><a style=\"float: left\" href=\"files/functions/removeVouch.php?id=" . $row['ID'] . "\"><img src=\"http://bumblebee.ml/admin/files/css/No.png\"></a><a style=\"float: right; width: 85%; text-decoration: none;\" href=\"files/functions/acceptVouch.php?id=" . $row['ID'] . "\"><div class=\"acceptVouch\">Accept</div></a></td>";
						}
						echo "<tr>";
					}
			?>
				</tbody>
			</table>
			<table style="left: 400; bottom: 500;">
				<tr style="background-color: black;">
					<?php
					for ($i = 1; $i <= $maxPages; $i++) {
						echo "<td><a style=\"text-decoration: none; color: white;\" href=\"vouches.php?page=" . $i . "\"><div class=\"page\">" . $i . "</div></a></td>";
					}
					?>
				</tr>
			</table>
			<?php
				?>

		</div>
	</div>
</body>
</html>