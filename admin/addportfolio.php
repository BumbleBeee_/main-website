<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">
<style>
	.addportfolio {
		background-color: #4CAF50;
	}
	.addportfolio:hover {
		background-color: #4CAF50 !important;
	}

</style>
<head>
	<meta charset="UTF-8">
	<title>Add A Project | Bumblebee.ml</title>
	<link rel="stylesheet" href="files/css/styles.css">
	<link rel="stylesheet" href="files/css/contact.css">
</head>
<body style="background-image: url('files/css/bg2.png'); color: white;">
	<?php
	if (!($_SESSION["username"] === "admin")) {
		header("Location: http://bumblebee.ml");
		die();
	}
	include("files/adminnav.php");
	include("../files/connect.php");
	?>
	<div style="padding-left: 30%; padding-top: 150px;">
		
		<h1>Add A Project</h1>
		<p></p>
		<?php
		if (isset($_POST['submit'])) {
			if (isset($_POST['name']) && isset($_POST['type']) && sizeof(split(" ", $_POST['desc'])) > 1) {
				$name = $_POST['name'];
				$type = $_POST['type'];
				$for = "";
				if (isset($_POST['for'])) {
					$for = $_POST['for'];
				}
				$desc = $_POST['desc'];
				
				$query = "INSERT INTO Portfolio (Name, Type, MadeFor, Description) VALUES 
				('$name', '$type', '$for', '$desc')";
				if (mysqli_query($con, $query) !== TRUE) {
					?>
					<p style="color: red;">Error: Failed to submit</p>
					<?php
				} else {
					?>
					<p style="color: green;">Successfully added project!</p>
					<?php
				}
			} else {
				?>
				<p style="color:red;">You must fill out all the fields</p>
				<?php
			}
		}
		?>
		<form action="addportfolio.php" method="POST">
			<strong>Project Name</strong> <span style="color: gray">(required)</span> <br><input type="text" name="name"><br>
			<br>
			<strong>Type</strong><br><input type="text" name="type"><br>
			<br>
			<strong>Made For</strong><br><input type="text" name="for"><br>
			<br>
			<strong>Description</strong><br><textarea name="desc" cols="80" rows="20"></textarea>
			<br>
			<br><br>
			<input name="submit" type="submit" value="Submit">
		</form>
	</div>
</body>
</html>