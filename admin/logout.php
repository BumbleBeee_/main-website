<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Logout | Bumblebee.ml</title>
</head>
<body>
	<?php
	if (!($_SESSION["username"] === "admin")) {
		header("Location: http://bumblebee.ml");
		die();
	}
	if ($_SESSION["username"] == "admin") {
		session_unset();
		session_destroy();
		header("Location: http://bumblebee.ml/admin/index.php");
	}
	?>
</body>
</html>