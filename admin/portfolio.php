<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">
<style>
	.portfolio {
		background-color: #4CAF50;
	}
	.portfolio:hover {
		background-color: #4CAF50 !important;
	}
</style>
<head>
	<meta charset="UTF-8">
	<title>Portfolio | Bumblebee.ml</title>
	<link rel="stylesheet" href="files/css/styles.css">
</head>
<body style="background-image: url('files/css/bg2.png'); color: white;">
	<?php
	if (!($_SESSION["username"] === "admin")) {
		header("Location: http://bumblebee.ml");
		die();
	}
	$_SESSION['returnurl'] = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
	include("files/adminnav.php");
	include("../files/connect.php")
	?>

	<div class="contactwrapper">
		<div class="contactme">
		<?php
		$page = $_GET['page'];
		if ($page === NULL) {
			$page = 1;
		}
		$offset = ($page*4)-4;

		$query = "SELECT * FROM Portfolio LIMIT ". $offset . ", 5";
		$result = mysqli_query($con, $query);

		$maxQuery = "SELECT ID FROM Portfolio";
		$maxResult = mysqli_query($con, $maxQuery);
		$maxPages = ceil((mysqli_num_rows($maxResult)/5));

			?>
			<table class="contactData" rules="none" style="width: 100%; table-layout:relative;">
				<thead>
					<tr>
						<td>ID</td>
						<td>Name</td>
						<td>Type</td>
						<td>For</td>
						<td>Description</td>
						<td>Tools</td>
					</tr>
				</thead>
				<tbody>
					<?php
					while ($row = mysqli_fetch_assoc($result)) {
						echo "<tr class=\"contactData\">";
						echo "<td style=\"word-wrap: break-word\">" . $row['ID'] . "</td>";
						echo "<td style=\"word-wrap: break-word\">" . $row['Name'] . "</td>";
						echo "<td style=\"word-wrap: break-word\">" . $row['Type'] . "</td>";
						echo "<td style=\"word-wrap: break-word\">" . $row['MadeFor'] . "</td>";
						echo "<td style=\"word-wrap: break-word\">" . $row['Description'] . "</td>";
						echo "<td>
							<a href=\"files/functions/removeProject.php?id=" . $row['ID'] . "\">
							<img src=\"http://bumblebee.ml/admin/files/css/No.png\"></a>
							<a href=\"files/functions/editProject.php?id=" . $row['ID'] . "\">
							<img src=\"http://bumblebee.ml/admin/files/css/Modify.png\"></a>
							</td>";
						echo "<tr>";
					}
			?>
			</tbody>
			</table>
			<table style="left: 400; bottom: 500;">
				<tr style="background-color: black;">
					<?php
					for ($i = 1; $i <= $maxPages; $i++) {
						echo "<td><a style=\"text-decoration: none; color: white;\" href=\"portfolio.php?page=" . $i . "\"><div class=\"page\">" . $i . "</div></a></td>";
					}
					?>
				</tr>
			</table>
			<?php
				?>

		</div>
	</div>
</body>
</html>