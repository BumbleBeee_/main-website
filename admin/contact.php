<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">
<style>
	.contact {
		background-color: #4CAF50;
	}
	.contact:hover {
		background-color: #4CAF50 !important;
	}
</style>
<head>
	<meta charset="UTF-8">
	<title>Contact | Bumblebee.ml</title>
	<link rel="stylesheet" href="files/css/styles.css">
</head>
<body style="background-image: url('files/css/bg2.png'); color: white;">
	<?php
	if (!($_SESSION["username"] === "admin")) {
		header("Location: http://bumblebee.ml");
		die();
	}
	include("files/adminnav.php");
	include("../files/connect.php")
	?>
	<div class="contactwrapper">
		<div class="contactme">
		<?php 
		$query = "SELECT * FROM Contact";
		$result = mysqli_query($con, $query);
			?>
			<table class="contactData" width="100%" rules="none">
				<thead>
					<tr>
						<td>ID</td>
						<td>Name</td>
						<td>Email</td>
						<td>Message</td>
					</tr>
				</thead>
				<tbody>
					<?php
					while ($row = mysqli_fetch_assoc($result)) {
						echo "<tr class=\"contactData\">";
						echo "<td>" . $row['ID'] . "</td>";
						echo "<td>" . $row['Name'] . "</td>";
						echo "<td>" . $row['Email'] . "</td>";
						echo "<td>" . $row['Message'] . "</td>";
						echo "<tr>";
					}
			?>
				</tbody>
			</table>
			<?php
				?>

		</div>
	</div>
</body>
</html>