<?php 
session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Admin | Bumblebee.ml</title>
	<link rel="stylesheet" href="files/css/styles.css">
</head>
<body style="background-image: url('files/css/bg2.png'); color: white;">
	<?php include("files/adminnav.php"); ?>
	<div class="wrapper">
		<div class="login">
			<div class="loginwrapper">
				<?php
				if (isset($_POST['submit'])) {
					if (isset($_POST['username']) && isset($_POST['password'])) {
						$pass = $_POST['password'];
						$user = $_POST['username'];
						if ($user === "admin") {
							if (password_verify($pass, '$2y$10$CtEyPlpktTsNQc79GRFzceab76VSfVMOpdjQU0xrPzLdIXcBL2ftm')) {
								$_SESSION["username"] =  "admin";
								?>
								<p style="color: green;">Successfully logged in!</p>
								<?php
							} else {
								?>
								<p style="color: red;">Invalid password</p>
								<?php
							}
						} else {
							?>
						<p style="color: red;">Invalid username</p>
						<?php
						}
					} else {
						?>
						<p style="color: red;">You must provide a username and password</p>
						<?php
					}
				}
				?>
				<form action="index.php" method="POST">
					Username
					<br>
					<input type="text" name="username" placeholder="Username">
					<br>
					<br>
					Password
					<br>
					<input type="password" name="password" placeholder="Password">
					<br>
					<br>
					<input type="submit" value="Submit" name="submit">
				</form>
			</div>
		</div>
	</div>
</body>
</html>