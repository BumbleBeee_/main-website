<?php 
session_start();
?>
<div class="navbar">
	<ul>
		<a class="website" href="http://bumblebee.ml/">
			<li>Website</li>
		</a>
		<a class="contact" href="http://bumblebee.ml/admin/contact.php">
			<li>Contact</li>
		</a>
		<a class="clients" href="http://bumblebee.ml/admin/clients.php">
			<li>Clients</li>
		</a>
		<a class="portfolio" href="http://bumblebee.ml/admin/portfolio.php">
			<li>Portfolio</li>
		</a>
		<a class="addportfolio" href="http://bumblebee.ml/admin/addportfolio.php">
			<li>Add Project</li>
		</a>
		<a class="vouch" href="http://bumblebee.ml/admin/vouches.php">
			<li>Vouches</li>
		</a>
		<?php
		if ($_SESSION["username"] === "admin") {
			?>
			<a href="http://bumblebee.ml/admin/logout.php">
				<li>Logout</li>
			</a>
			<?php
		}
		?>
	</ul>
</div>