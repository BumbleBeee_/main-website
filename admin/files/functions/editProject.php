<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Edit Project | Bumblebee.ml</title>
	<link rel="stylesheet" href="../css/styles.css">
</head>
<body style="background-image: url('../css/bg2.png'); color: white;">
	<?php
	if (!($_SESSION["username"] === "admin")) {
		header("Location: http://bumblebee.ml");
		die();
	}
	include("../adminnav.php");
	include("../../../files/connect.php");
	$id = $_GET['id'];
	if (isset($_POST['submit'])) {
		if (isset($_POST['name']) && isset($_POST['type']) && sizeof(split(" ", $_POST['desc'])) > 1) {
			$name = $_POST['name'];
			$type = $_POST['type'];
			$for = "";
			if (isset($_POST['for'])) {
				$for = $_POST['for'];
			}
			$desc = $_POST['desc'];
			
			$query = "UPDATE Portfolio SET Name=\"$name\", Type=\"$type\", MadeFor=\"$for\", Description=\"$desc\"";
			if (mysqli_query($con, $query) !== TRUE) {
				?>
				<p style="color: red;">Error: Failed to update</p>
				<?php
			} else {
				?>
				<p style="color: green;">Successfully updated project!</p>
				<?php
			}
		} else {
			?>
			<p style="color:red;">You must fill out all the fields</p>
			<?php
		}
		header("Location: $_SESSION[returnurl]");
		$_SESSION['returnurl'] = "";
		die();
	}


	$query = "SELECT * FROM Portfolio WHERE ID=" . $id;
	$res = mysqli_query($con, $query);
	$row = mysqli_fetch_assoc($res);

	$name = $row['Name'];
	$type = $row['Type'];
	$for = $row['MadeFor'];
	$desc = $row['Description'];
	echo($desc);
	?>
		<div style="padding-left: 30%; padding-top: 150px;">
			<form action="editProject.php?id=$id" method="POST">
				<strong>Project Name</strong> <span style="color: gray">(required)</span> <br><input type="text" name="name" <?php echo "value=\"$name\""; ?>><br>
				<br>
				<strong>Type</strong><br><input type="text" name="type" <?php echo "value=\"$type\""; ?>><br>
				<br>
				<strong>Made For</strong><br><input type="text" name="for" <?php echo "value=\"$for\""; ?>><br>
				<br>
				<strong>Description</strong><br><textarea name="desc" cols="80" rows="20"><?php echo $desc; ?></textarea>
				<br>
				<br><br>
				<input name="submit" type="submit" value="Submit">
			</form>
		</div>
	<?php
	?>
</body>
</html>