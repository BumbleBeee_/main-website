<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Remove Vouch | Bumblebee.ml</title>
	<link rel="stylesheet" href="../css/styles.css">
</head>
<body>
	<?php
	if (!($_SESSION["username"] === "admin")) {
		header("Location: http://bumblebee.ml");
		die();
	}
	include("../adminnav.php");
	include("../../../files/connect.php");
	echo "Please wait..";
	$id = $_GET['id'];
	$query = "DELETE FROM Vouches WHERE ID=" . $id;
	if (mysqli_query($con, $query) !== TRUE) {
		echo "Failed to remove vouch!";
		die();
	}
	header("Location: $_SESSION[returnurl]");
	$_SESSION['returnurl'] = "";
	die();
	?>
</body>
</html>