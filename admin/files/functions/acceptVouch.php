<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Accept Vouch | Bumblebee.ml</title>
	<link rel="stylesheet" href="../css/styles.css">
</head>
<body>
	<?php
	if (!($_SESSION["username"] === "admin")) {
		header("Location: http://bumblebee.ml");
		die();
	}
	include("../adminnav.php");
	include("../../../files/connect.php");
	echo "Please wait..";
	$id = $_GET['id'];
	$query = "UPDATE Vouches SET Status='ACCEPTED' WHERE ID=" . $id;
	if (mysqli_query($con, $query) !== TRUE) {
		echo "Failed to accepted vouch!";
		die();
	}
	header("Location: $_SESSION[returnurl]");
	$_SESSION['returnurl'] = "";
	die();
	?>
</body>
</html>