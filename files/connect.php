<?php

define("host", "localhost");
define("user", "root");
define("password", "nitram");
define("database", "WebsiteData");

$con = mysqli_connect(host, user, password, database) or die(mysql_error());

$exists = mysqli_query($con, "SHOW TABLES LIKE 'Stats'");
$contact = mysqli_query($con, "SHOW TABLES LIKE 'Contact'");
$vouchExists = mysqli_query($con, "SHOW TABLES LIKE 'Vouches'");
$portfolioExists = mysqli_query($con, "SHOW TABLES LIKE 'Portfolio'");
$cacheExists = mysqli_query($con, "SHOW TABLES LIKE 'CachedStats'");


if (mysqli_num_rows($exists) <  1) {
	$create = "CREATE TABLE Stats (
		GUID VARCHAR(128) NOT NULL PRIMARY KEY,
		OS VARCHAR(128) NOT NULL,
		OSVersion VARCHAR(128) NOT NULL,
		Arch VARCHAR(128) NOT NULL,
		Cores VARCHAR(128) NOT NULL,
		ServerVersion VARCHAR(128) NOT NULL,
		JavaVersion VARCHAR(128) NOT NULL,
		Players VARCHAR(128) NOT NULL,
		PluginVersion VARCHAR(128) NOT NULL,
		OnlineMode VARCHAR(128) NOT NULL,
		Status VARCHAR(128) NOT NULL
		)";
	if ($con->Query($create) === TRUE) {
		echo "Error 1";
	}
}


if (mysqli_num_rows($contact) < 1) {
$create = "CREATE TABLE Contact (
	ID INT(128) NOT NULL PRIMARY KEY AUTO_INCREMENT,
	Name VARCHAR(128) NOT NULL,
	Email VARCHAR(128) NOT NULL,
	Message VARCHAR(128) NOT NULL
	)";

	if ($con->Query($create) !== TRUE) {
		echo "Error 2";
	}
}

if (mysqli_num_rows($vouchExists) < 1) {
$vouches = "CREATE TABLE Vouches (
	ID INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	Username VARCHAR(128) NOT NULL,
	ServerName VARCHAR(128) NOT NULL,
	ServerIP VARCHAR(128),
	Message VARCHAR(1024) NOT NULL,
	Status VARCHAR(128) NOT NULL
	)";

	if ($con->Query($vouches) !== TRUE) {
		echo "Error 3";
	}
}

if (mysqli_num_rows($portfolioExists) < 1) {
$portfolio = "CREATE TABLE Portfolio (
	ID INT(128) NOT NULL PRIMARY KEY AUTO_INCREMENT,
	Name VARCHAR(128) NOT NULL,
	Type VARCHAR(128) NOT NULL,
	MadeFor VARCHAR(128) NOT NULL,
	Description VARCHAR(128) NOT NULL
	)";

	if ($con->Query($portfolio) !== TRUE) {
		echo "Error 4";
	}
}

if (mysqli_num_rows($cacheExists) < 1) {
$cache = "CREATE TABLE CachedStats (
	Plugin VARCHAR(128) NOT NULL PRIMARY KEY,
	TotalPlayers VARCHAR(128) NOT NULL,
	TotalOnline VARCHAR(128) NOT NULL,
	TotalServers VARCHAR(128) NOT NULL
	)";

	if ($con->Query($cache) !== TRUE) {
		echo "Error 5";
	}
}
?>