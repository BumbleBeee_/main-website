<style>
.navbar {
	margin: 0;
}

.navbar ul {
    list-style-type: none;
    margin: 0;
    padding: 0;
    overflow: hidden;
    background-color: #333;
}

.navbar li {
	display: inline-block;
    color: white;
    text-align: center;
    padding: 20px;
    text-decoration: none;
    font-family: verdana;
}

.navbar a {
	float: left;
    margin: 0;
}

.navbar a:hover {
	text-decoration: none;
	background-color: black;
}

.icon {
	display: none;
}


@media screen and (max-width:720px) {
  .navbar a:not(:first-child) {display: none;}
  .icon {
    float: right !important;
    padding-right: 0;
    display: inline-block !important;
  }
}

@media screen and (max-width:720px) {
  .responsive a {
  	text-decoration: none;
  	padding: 5px;
    float: none !important;
    display: block !important;
  }
  .responsive li {
    display: block !important;
    text-align: left !important;
  }
  .responsive ul {
  	width: 100% !important;
  }
}
</style>

<center>
<div class="navbar" id="myTopnav">
	<ul>
		<a class="home" href="http://bumblebee.gq/index.php">
			<li>Home</li>
		</a>
		<a class="portfolio" href="http://bumblebee.gq/portfolio.php">
			<li>Portfolio</li>
		</a>
		<a class="contact" href="http://bumblebee.gq/contact.php">
			<li>Contact Me</li>
		</a>
		<a class="stats" href="http://bumblebee.gq/stats/index.php">
			<li>Plugin Stats</li>
		</a>
		<a class="vouch" href="http://bumblebee.gq/vouches.php">
			<li>Vouches</li>
		</a>
		<a href="http://bumblebee.gq/addvouch.php" class="addvouch">
			<li>Add Vouch</li>
		</a>
		<a class="icon" href="javascript:open();">
			<li>&#9776;</li>
		</a>
	</ul>
</div>
</center>

<script>
function open() {
    var x = document.getElementById("myTopnav");
    if (x.className === "navbar") {
        x.className += " responsive";
    } else {
        x.className = "navbar";
    }
}
</script>
