<!DOCTYPE html>
<html lang="en">
<style>
	.portfolio {
		background-color: #4CAF50;
	}
	.portfolio:hover {
		background-color: #4CAF50 !important;
	}
</style>
<head>
	<meta charset="UTF-8">
	<title>Portfolio | Bumblebee.ml</title>
	<link rel="stylesheet" href="files/css/styles.css">
</head>
<body style="background-image: url('files/css/bg2.png'); background-repeat: repeat-x; color: white; background-color: #0c889c;">
	<?php include("files/navbar.php") ?>
	<?php include("files/connect.php") ?>
	<div class="vouchwrapper">
		<div class="vouches">
			<table width="100%">

				<?php

				$page = $_GET['page'];
				if ($page === NULL) {
					$page = 1;
				}
				$offset = ($page*4)-4;

				$query = "SELECT * FROM Portfolio ORDER BY ID DESC LIMIT " . $offset . ", 4";
				$result = mysqli_query($con, $query);

				$maxQuery = "SELECT * FROM Portfolio";
				$maxResult = mysqli_query($con, $maxQuery);
				$amount = mysqli_num_rows($maxResult);
				$maxPages = ceil(($amount/4));

				while ($row = mysqli_fetch_assoc($result)) {
					?>
					<tr class="project">
						<td>
							Name: <?php echo $row['Name'] ?>
							<br><br>
							Type: <?php echo $row['Type'] ?>
							<br><br>
							Made For: <?php echo $row['MadeFor'] ?>
							<br><br>
							Description:
							<br><br>
							<textarea style="width: 80%;" cols="51" rows="11" readonly="true"><?php echo $row['Description']; ?></textarea>
						</td>
					</tr>
					<tr style="height: 10px;"></tr>
					<?php
				}
				?>
			</table>
			<br>
			<br>
			<br>
			<table style="position: absolute; left: 0; bottom: 0;">
				<tr style="background-color: black;">
					<?php
					for ($i = 1; $i <= $maxPages; $i++) {
						echo "<td><a style=\"text-decoration: none; color: white;\" href=\"portfolio.php?page=" . $i . "\"><div class=\"page\">" . $i . "</div></a></td>";
					}
					?>
				</tr>
			</table>
		</div>
	</div>
</body>
</html>